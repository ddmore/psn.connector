@echo off


set PREAMBLE="${job.commandExecutionTarget.environmentSetupScript}"
for %%F in (%PREAMBLE%) do set PREAMBLE_DIR=%%~dpF
cd "%PREAMBLE_DIR%"

CALL %PREAMBLE%


set WORK_DIR="${job.jobWorkingDirectory}"
for %%D in (%WORK_DIR%) do set WORK_DIR_DRIVE=%%~dD
echo Job Working Directory = %WORK_DIR%
echo Job Working Directory Drive = %WORK_DIR_DRIVE%
cd %WORK_DIR%
%WORK_DIR_DRIVE%


<#assign executionFile = job.executionFileName>
<#if (job.executionFileName.endsWith(".xml")) >
    <#assign executionFile = job.executionFileName.replace(".xml",".ctl")>
</#if>

<#assign psnlogFile = (job.executionFileName)?replace("\\.[A-Za-z0-9]{1,3}$", ".psn.log", "r")>

CALL ${job.executionRequest.executionParameters!} "${executionFile}" -directory=rundir 1> "${psnlogFile}" 2>&1

exit 0






