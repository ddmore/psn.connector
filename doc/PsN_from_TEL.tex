This document describes how PsN can be run from the TEL command-line.
All TEL functions described below take an estimation mdl-file or -object as input. 
Any estimation-type mdl-file (or perhaps -object if the IDE supports that) 
that is correcly translated by the mdl-to-PharmML and PharmML-to-NMtran converters can be used. 
However, some PsN-features are not suitable for use with
mdl-models which have variability parameters with type=SD, see details below.

There is an alternative route for the case when translation
mdl-PharmML-NMtran does not work. Then the mdl-file in the
TEL-command can be replaced with an NMtran file with extension ''.ctl''.
Please refer to Mango for details regarding this functionality.

\section{General considerations}
\subsection*{PsN command-line options}
It is possible to set almost any PsN command-line option in TEL, see examples below
for estimate, bootstrap.PsN, SSE.PsN and VPC.PsN. However, the extra options are not parsed
and interpreted until PsN is started by the framework after a number of processing steps. 
This means it is important to check spelling carefully when setting extra PsN options, 
because there will be a \emph{considerable} delay
before getting back an SO containing an error message of the type ``Unknown option: pikcy''.
%There are literally hundreds of options to PsN, and the validity of many of them depend on a complex
%combination of command, settings of other options and the contents of the NMtran control stream, so it is
%not feasible to have option checking on the TEL side without actually running PsN.

PsN option -directory is unavailable, and so are
all restart/resume/reanalyze features that are linked to this option. This
is due to the design of the framework.

The PsN version is 4.4.8 and all user guides are found via web page
\texttt{http://psn.sourceforge.net/docs.php}, for example\\
\texttt{http://psn.sourceforge.net/pdfdocs/vpc\_npc\_userguide.pdf}

\subsection*{Finding intermediate output while PsN is still running}
Note: This section only applies in the SEE, when the servers are running on 
the local computer. In a production setting the trick described here
is not possible.

PsN is run 
in a temporary folder with the name of the job printed in the TEL command-window. 
The place of the temporary folder depends on the system, but can be \verb| C:\Users\name\AppData\Local\Temp\mifshare |
Example, if user name is kajsa and the job name
is 10dabf72-e7c1-4048-a89d-d790fb8cb84e (TEL message ``Job 10dabf72-e7c1-4048-a89d-d790fb8cb84e is NEW'')
then the PsN run folder is 
\begin{verbatim}
C:\Users\kajsa\AppData\Local\Temp\mifshare\
10dabf72-e7c1-4048-a89d-d790fb8cb84e\rundir
\end{verbatim}

\subsection*{Monitoring the progress of a running PsN job}
Note: This only applies in the SEE, when the servers are running on 
the local computer.

The PsN progress messages are written to a file, named after the MDL file, 
in the temporary folder above the PsN run folder.
Example, if user name is kajsa, the MDL file is called
UseCase1.mdl and the job name is 10dabf72-e7c1-4048-a89d-d790fb8cb84e
then the log file is 
\begin{verbatim}
C:\Users\kajsa\AppData\Local\Temp\mifshare\
10dabf72-e7c1-4048-a89d-d790fb8cb84e\UseCase1.psn.log
\end{verbatim}
and can be viewed with
\begin{verbatim}
cd C:\Users\kajsa\AppData\Local\Temp\mifshare
type 10dabf72-e7c1-4048-a89d-d790fb8cb84e\UseCase1.psn.log
\end{verbatim}


\subsection*{TEL command-line timeout}
If a message similar to 
\begin{verbatim}
Operation timed out after 500 milliseconds with 0 bytes received
\end{verbatim}
appears in the TEL window, it means that TEL has lost contact with the running job, but that the job might still be
running. Check in the temporary job folder (see ``Monitoring the progress of a running PsN job'' above)
to see if job output can be found there.

\section{TEL estimate()}
The estimate() function in TEL can be run with target=''PsN'', for example
\begin{verbatim}
mySO <- estimate("tumour.mdl",target="PsN",subfolder="run1")
\end{verbatim}
The output is a populated SO object. 
The subfolder option to the estimate function can only be used to give an informative name to the 
folder where the SO.xml and other files are placed. It cannot be used in the same
way as the PsN 'directory' option. 

It is possible to add PsN execute command-line options with the addargs option to estimate():
\begin{verbatim}
mySO <- estimate("tumour.mdl",target="PsN",subfolder="run1",
addargs="-picky -retries=4")
\end{verbatim}

\subsubsection*{Warnings}
The -min\_retries and -retries functionality of PsN is only intended for models where 
variability parameters are encoded with type=VAR (see MCL specification on Parameter Object - Variability block - Variability attributes). 
If type=SD (the default) then the perturbation of initial estimates that is part of the
-min\_retries and -retries functionality might not be very useful, but the final results 
in the returned SO object will be correct.
An automatic input check for type=SD in combination with -min\_retries or -retries is not implemented yet.

\section{TEL bootstrap.PsN}
The bootstrap.PsN function 
returns an SO object. 
If the lst-file from a NONMEM estimation of the model is
already available, it will save run time to include it with option extraInputFiles (the file needs to be in the same folder 
as the mdl-file),
but this is optional. Including the lst-file is fine even when
when variability parameters in the mdl
(see MCL specification on Parameter Object - Variability block - Variability attributes)
were encoded with type=SD.
Example:
\begin{verbatim}
bootstrapResults <- bootstrap.PsN("tumour.mdl", samples=100, seed=87653,
bootstrapOptions=" -skip_minimization_terminated", 
extraInputFiles=c("tumour.lst"),
subfolder="bootstrap_tumour", plot=TRUE)
\end{verbatim}
The only required input is the model, 'samples' and 'seed'. 
Additional optional PsN bootstrap options can be passed on via 'bootstrapOptions'. This is a text string that will be
passed on unchanged as input to PsN bootstrap. The options should be written exactly as they would be on the bootstrap command-line.
Including PsN option directory in 'bootstrapOptions' string will have no effect. A selected set of bootstrap output files,
including PsN's bootstrap\_results.csv, will be copied to the folder set with the 'subfolder' option. The subfolder option cannot
be used to restart a bootstrap run that ended prematurely. If option subfolder is not set, the subfolder will
be given a default name which is bootstrap\_someTimestamp, for example bootstrap\_2014Dec12111254.

The SO object will usually not contain individual parameter estimates, and this is as expected.

The plot option indicates whether the Xpose boot.hist function should be used to visualize the bootstrap results.
Those plots will appear in the TEL panel of the MDL-IDE, R graphics tab.
If plot=TRUE but the xpose4 package is not installed then there will be an error.

If the user wishes to plot the results at a later time, the names of the files needed can be obtained from the SO object:
\begin{verbatim}
bootstrapResults@RawResults@DataFiles$PsN_bootstrap_results$path
bootstrapResults@RawResults@DataFiles$PsN_bootstrap_included_individuals$path
bootstrapResults@RawResults@DataFiles$PsN_bootstrap_raw_results$path
\end{verbatim}

If it is not possible to use ``page up'' and ``page down'' to change page in a multi-page plot, it is recommended to run
command \verb|windows(record=TRUE)|
and then the bootstrap plot command
\verb|boot.hist(results.file=....|
Then there should appear a new window (it might be behind the IDE)
with the bootstrap plots, where page up and page down lets you flip between pages of the plot.


\subsubsection*{Warnings}
When variability parameters in the mdl
(see MCL specification on Parameter Object - Variability block - Variability attributes)
were encoded with type=SD, PsN will compute the results, including percentiles, on the
variance scalar, and then the nmoutput2so program will attempt to translate them
to SD scale when creating the SO xml-file. Not all results can be converted this way, so some
values will be missing. See details in the nmoutput2so documentation.

The -min\_retries and -retries functionality of PsN is only intended for models where 
variability parameters are encoded with type=VAR 
(see MCL specification on Parameter Object - Variability block - Variability attributes). 
If type=SD (the default) then the perturbation of initial estimates that is part of the
-min\_retries and -retries functionality might not be very useful, but the final results 
in the returned SO object will be correct.
An automatic input check for type=SD in combination with -min\_retries or -retries is not implemented yet.


\section{TEL VPC.PsN}
Before running VPC.PsN the user must either use the TEL update() function to 
set the parameter values in the mdl file/object to what is desired for simulation.
It is also possible to set options to ensure that the relevant lst-file is copied to the run folder,
but this must not be done if variability parameters in the mdl
(see MCL specification on Parameter Object - Variability block - Variability attributes)
were encoded with type=SD. 
The lst-file from a
previous estimation with NONMEM will not be automatically used as input to the vpc.

VPC.PsN returns 
an SO object.
%(This is new compared to the older VPC.PsN which would return
%a list  with the SO and a set of files to be used with the xpose.VPC function.)
%A required option seed has been added.
Example, not using any lst-file input:
\begin{verbatim}
vpcResults <- VPC.PsN("tumour.mdl", samples=100, seed=76543,
vpcOptions=" -stratify_on=DOSE -auto_bin=10", 
subfolder="vpc_tumour", plot=TRUE)
\end{verbatim}

%xpose.VPC(vpc.info=vpcFiles$vpc.info,vpctab=vpcFiles$vpctab,main="VPC tumour")
%The \$vpc.info and \$vpctab elements in the list are absolute file paths to the relevant files.
%The \$SO element of the list is the SO object.

The only required input is the model, 'samples' and 'seed'. When running standalone-PsN the seed
can be any string, but in VPC.PsN the seed must be an integer. Note that using the same seed 
guarantees the same simulated samples, but that using a different seed does not guarantee
a different set of simulated samples, that must be verified.

Additional optional PsN vpc options can be passed on via 'vpcOptions'. This is a text string that will be
passed on unchanged as input to PsN vpc. The options should be written exactly as they would be on the vpc command-line.
Including PsN option directory in 'vpcOptions' string will have no effect. A selected set of vpc output files,
including PsN's vpc\_results.csv and a NONMEM table file npctab.dta with ''samples'' simulated sets of observations, 
will be copied to the folder set with the 'subfolder' option. The subfolder option cannot
be used to redo the binning in an earlier run. If option subfolder is not set, the subfolder will
be given a default name that includes a timestamp, for example vpc\_2014Dec12111254.

The plot option indicates whether the Xpose xpose.vpc function should be used to visualize the results.
If plot=TRUE but the xpose4 package is not installed then there will be an error.
If the user wants to plot the vpc at a later time, the names of the two files needed are in the RawResults section of the SO:
\begin{verbatim}
vpcResults@RawResults@DataFiles$PsN_VPC_results$path
vpcResults@RawResults@DataFiles$PsN_VPC_vpctab$path
\end{verbatim}

Setting stratify\_on in 'vpcOptions' will only work for variables that will be in \$INPUT of the 
converter-generated NMtran file. Try setting stratify\_on to the name of the MDL input variable and see if
it works, otherwise check the NMtran file to see what the name if the variable should be.

\subsubsection*{vpc for count/categorical data}
The simplest way of running a vpc is to give and estimation model as input. PsN will then automatically copy
the input estimation model and modify it to a simulation model instead, via the steps described in \\
\texttt{http://psn.sourceforge.net/pdfdocs/vpc\_npc\_userguide.pdf}

As described in the vpc userguide, when creating a simulation model 
based on the estimation model, PsN is only capable of a simple adding of \$SIM,
and optional keeping/removal of  \$EST (vpc option -keep\_estimation) and optional adding of
NOPRED in \$SIM (vpc option -noprediction). For count/categorical data more
changes are \emph{usually} needed, and in that case a separate simulation model must be given as input
to vpc. 

To investigate whether a separate simulation is needed, it is best to run a vpc with PsN outside of the SEE.
Try giving only the estimation NMtran file, as it would be written by the PharmML->NMtran converter,
as input to vpc in combination with the other options (usually -levels for count/categorical data)
and see what happens.  
There is almost a 1-to-1 mapping of the options needed on the vpc commandline, and the
vpcOptions set in the VPC.PsN TEL command.

In the SEE the only possible alternative for passing a separate simulation model is vpc option -sim\_model.
The framework does not support translation and passing of two mdl-models as input to PsN,
so the simulation model must already exist in NMtran format, and with the data file name in \$DATA  
\emph{without path and the same data file name as the estimation model after translation to NMtran}.
Assuming such an NMtran file for simulation exists in the same folder as the 
MDL file categorical.mdl
and is called categoricalSim.ctl, then the TEL command can be
\begin{verbatim}
vpcResults <- VPC.PsN("categorial.mdl", samples=100, seed=123,
vpcOptions=" -sim_model=categoricalSim.ctl -levels=0.5,1.5,2.5 -auto_bin=10",
extraInputFiles=c("categoricalSim.ctl"),
) 
\end{verbatim}
Note that the file name must match between -sim\_model
and extraInputFiles.


\subsubsection*{Not yet tested}
It is possible to use the -lst\_file option to vpc with a specific file. In vpc\_options the
-lst\_file option should be added with the file name, and
then option extraInputFiles must also be added with the lst-file, which needs to be in the same folder as the
mdl-file. 
\begin{verbatim}
vpcResults <- VPC.PsN("tumour.mdl", samples=100, seed=76543,
vpcOptions=" -stratify_on=DOSE -auto_bin=10 -lst_file=estimation.lst",
extraInputFiles=c("estimation.lst"),
) 
\end{verbatim}
{\bf Warning:} Using the lst-file as input will introduce errors if the input NMtran file
created by the converters have \$OMEGA and/or \$SIGMA encoded as STANDARD/CORRELATION.

Automatic inclusion of files set with option -lst\_file into vector extraInputFiles is not yet implemented.


\subsubsection*{Not yet tested}
An alternative, but more risky, approach to getting the lst-file included as vpc input is
to turn on automatic copying of lst-files that exist in the same folder
as the input MDL-file. This is done using option extraInputFileExts. Example:
\begin{verbatim}
vpcResults <- VPC.PsN("tumour.mdl", samples=100, seed=76543,
extraInputFileExts=c("lst"),
vpcOptions=" -stratify_on=DOSE -auto_bin=10") 
\end{verbatim}
{\bf Warning:} Using the lst-file as input will introduce errors if the input NMtran file
created by the converters have \$OMEGA and/or \$SIGMA encoded as STANDARD/CORRELATION.

Please note that the option extraInputFileExts will have no effect if no lst-files exist in the
MDL model folder (perhaps they are in a subfolder), or if they exist but do not have the same base name as the model file.

%It should be avoided to set -lst\_file in vpcOptions, because it will only work under certain conditions. 
%It is better to make sure that the parameter values set in the input mdl-file already are the ones you want to use, 
%i.e. the final estimates from a previous estimation run. In that case the -lst\_file option is not needed.
%If the -lst\_file option is included in vpcOptions, the lst-file must be found in the same folder as the mdl-file, 
%the lst-file name must end with '.lst', and the lst-file name must be given without a path. Example
%\begin{verbatim}
%vpcResults <- VPC.PsN("tumour.mdl", samples=100, 
%vpcOptions="-auto_bin=10 -seed=543 -lst_file=tumour.lst", 
%subfolder="vpc_tumour")
%\end{verbatim}

\subsubsection*{Not yet tested}
Currently extra input files can be used, but they must be listed
both in vpcOptions 
%\emph{without path} 
and in
extraInputFiles 
%\emph{with path relative to the MDL} 
as in the example above.
This includes options -lst\_file, -rawres\_input, -sim\_model, -sim\_table
and -orig\_table.
Automatic inclusion of file names from input file options, such as -lst\_file, into vector extraInputFiles is not yet implemented.

\section{TEL SSE.PsN}
It is possible to call PsN sse from the TEL command-line, but option -alternative\_models is not supported
unless pre-converted nmtran is used, and the alternative model files passed using option extraInputFiles.
The function returns 
an SO object.
%(This is new compared to the older SSE.PsN which would return
%a list with an SO object and some output files.)
Example:
\begin{verbatim}
sseResults <- SSE.PsN("tumour.mdl", samples=10, seed=76543,
sseOptions=" -no-estimate_simulation", 
subfolder="sse_sim")
\end{verbatim}

The -no-estimate\_simulation option makes sse not estimate the input model using the simulated data sets. Please refer to the
sse documentation for how sse modifies the estimation input nmtran code to simulation nmtran code. With the above command, 10 simulated data sets
will be created as individual NONMEM table files, which are copied back to the sse\_sim subfolder. 

If option -samples=10, there will be 10 files
mc-sim-1.dat, mc-sim-2.dat,... with one simulated data set each in the format of a NONMEM table.
Each NONMEM table will have the same set of columns as the input data set.
If option subfolder is not set, the subfolder will
be given a default name that includes a timestamp, for example sse\_2014Dec12111254.

\subsubsection*{Not yet tested}
Currently extra input files can be used, for example -rawres\_input,
but they must be listed
both in sseOptions 
%\emph{without path} 
and in
extraInputFiles 
%\emph{with path relative to the MDL} 
as in the example for VPC.PsN.

\section{Running other PsN tools}
It is possible to run other PsN tools, for example llp, from TEL, but the SO generation for this has
not been fully implemented yet. The most interesting output will be PsN results files,
such as the raw\_results csv-file, copied back to
the TEL subfolder. The TEL function to use is called execute (not to be confused with the PsN tool execute),
option target="PsNgeneric" must be set,
and the whole PsN command except the model file name is given as a string with option addargs. Example for running llp:
\begin{verbatim}
llpSO <- execute("mymodel.mdl", target="PsNgeneric", 
addargs="llp -thetas=1,2 -rse_thetas=10,20", subfolder="llpMyModel")
\end{verbatim} 

